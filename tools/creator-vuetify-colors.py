import json

colors = [
    'red',
    'pink',
    'purple',
    'deep-purple',
    'indigo',
    'blue',
    'light-blue',
    'cyan',
    'teal',
    'green',
    'light-green',
    'lime',
    'yellow',
    'amber',
    'orange',
    'deep-orange',
    'brown',
    'blue-grey',
    'grey',
    'black',
    'white'
]

shades = [
    'darken-1',
    'darken-2',
    'darken-3',
    'darken-4',
    'darken-5',
    'lighten-1',
    'lighten-2',
    'lighten-3',
    'lighten-4',
    'accent-1',
    'accent-2',
    'accent-3',
    'accent-4'
]

vuetify_colors = []
for color in colors:
    vuetify_colors.append(
        {
            "name": '{}'.format(color),
            "value": '{}'.format(color)
        }
    )

vuetify_shades = []
for shade in shades:
    vuetify_shades.append(
        {
            "name": '{}'.format(shade),
            "value": '{}'.format(shade)
        }
    )

print(vuetify_colors)
print('----------')
print(vuetify_shades)

# with open('../json/idx20_cities-ready.json', 'a') as the_file:
#     the_file.write(newCitiesSorted)
