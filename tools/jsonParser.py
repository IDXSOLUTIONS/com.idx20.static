import json
import codecs
import re


# read file
# with open("../json/idx20-SubdivisionName.json", 'r') as myfile:
#     data = myfile.read()

obj = json.load(codecs.open("../json/idx20-listing-City.json", 'r', 'utf-8-sig'))

# parse file
# obj = json.loads(data)
# obj.sort()

# print ("OBJ: {}".format(obj))
newItems = []
for item in obj:
    # print(item.get('name'))
    # theValue = item.get('value')
    # theName = item.get('name')
    # typeIs = ""
    #
    # if isinstance(theValue, int):
    #     typeIs = "Int"
    # else:
    #     typeIs = "String"

    # display = re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', name)
    # if display:
    #     displayIs = display
    # else:
    #     displayIs = name

    # displayIs = theName

    # newItems[theValue] = {
    #     "display": displayIs,
    #     "type": typeIs
    # }
    newItems.append({"text": item, "value": item})

# Remove duplicates
# newItems = list(dict.fromkeys(newItems))

# newItems.sort()
# newItemsSorted = []
# nc = None
# for item in newItems:
#
#     # print('ITEM', item)
#     deCC = re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', item)
#     # print('deCC', deCC)
#     deCC = " ".join(deCC)
#     # print('deCC', deCC)
#
#     if len(deCC) < 1:
#         deCC = item
#
#     nc = {
#         "name": deCC,
#         "value": item
#     }
#     newItemsSorted.append(nc)
#
# print(newItemsSorted)

with open('../json/temp.json', 'a') as the_file:
    # the_file.write(newItems)
    json.dump(newItems, the_file)
